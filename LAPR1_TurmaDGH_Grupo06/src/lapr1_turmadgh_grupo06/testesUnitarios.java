/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_turmadgh_grupo06;

import java.util.Formatter;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.calculoRCbib;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.conjuntoTestesConsist;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.dividir;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.melhorEscolha;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.testConsistCrit;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.calculoIC;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.maiorValProp;
import static lapr1_turmadgh_grupo06.LAPR1_TurmaDGH_Grupo06.nCrit;

import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 *
 * @author user
 */
public class testesUnitarios {

    public static void main(String[] args) {

        //TESTE DIVIDIR
        String string = "1/2";
        double expectedDouble = 0.5;
        double matriz1[][] = {{1, 0.25, 4, 0.16666666666}, {4, 1, 4, 0.25}, {0.25, 0.25, 1, 0.2}, {6, 4, 5, 1}};
        double matriz2[][] = {{1, 2, 5, 1}, {0.5, 1, 4, 0.25}, {0.2, 0.33333333333, 1, 0.25}, {1, 0.5, 4, 1}};
        int[] cont = new int[1];
        double vetPrio1[] = {0.1308, 0.2444, 0.0657, 0.5591};
        double vetPrio2[] = {0.4123, 0.2079, 0.0831, 0.2966};
        Formatter outt = new Formatter(System.out);
        boolean expectedIC1 = false;
        boolean expectedIC2 = true;

        //TESTE TESTECONSIST E CONJUNTOTESTECRIT
        String[] cabec = {"mc_criterios  Estilo  Confiabilidade  Consumo", "mcp_estilo  car1  car2 car3  car4", "mcp_confiabilidade  car1  car2  car3  car4", "mcp_consumo  car1  car2  car3  car4"};
        int expectedNmatrizNconsist = 2;
        int guarda = 0;
        int[] conta = new int[1];
        conta[0] = 0;
        double matrizes[][][] = new double[3][4][4];
        matrizes[0] = matriz1;
        matrizes[1] = matriz2;
        matrizes[2] = matriz1;
        double[][] matrizPrioridades = {{0.13078762589071866, 0.4123130894870025, 0.13078762589071866}, {0.24443179958643876, 0.20794225902921554, 0.24443179958643876}, {0.06570416982788117, 0.08311479181044398, 0.06570416982788117}, {0.5590764046949614, 0.29662985967333794, 0.5590764046949614}};
        double[] vetPrioCrit = {0.3202, 0.5571, 0.1226};
        double[][] matrizCompCrit = {{1.0000, 0.5000, 3.0000}, {2.0000, 1.0000, 4.0000}, {0.3333, 0.2500, 1.0000}};
        double[][] matNorm1Crit = {{0.0889, 0.0455, 0.2857, 0.1031}, {0.3556, 0.1818, 0.2857, 0.1546}, {0.0222, 0.0455, 0.0714, 0.1237}, {0.5333, 0.7273, 0.3571, 0.6186}};
        double[] prior1Crit = {0.1308, 0.2444, 0.0657, 0.5591};
        double[][] matNorm2Crit = {{0.3704, 0.5217, 0.3571, 0.4000}, {0.1852, 0.2609, 0.2857, 0.1000}, {0.0741, 0.0870, 0.0714, 0.1000}, {0.3704, 0.1304, 0.2857, 0.4000}};
        double[] prior2Crit = {0.4123, 0.2079, 0.0831, 0.2966};
        double[][] matNorm3Crit = matNorm1Crit;
        double[] prior3Crit = prior1Crit;
        double[][] normMatrizCompCrit = {{0.3000, 0.2857, 0.3750}, {0.6000, 0.5714, 0.5000}, {0.1000, 0.1429, 0.1250}};

        
        double [][][]matNormCrit=new double [matrizes.length][matrizes[0].length][matrizes[0].length];
        matNormCrit[0]=matNorm1Crit;
        matNormCrit[1]=matNorm2Crit;
        matNormCrit[2]=matNorm3Crit;
        double [][]priorCrit=new double [matrizes.length][matrizes[0].length];
        priorCrit[0]=prior1Crit;
        priorCrit[1]=prior2Crit;
        priorCrit[2]=prior3Crit;
        double pesoCrit[] = {0.3, 0.6, 0.1};
        //double pesoCrit[] = {0.25, 0.25, 0.25,0.25}
        
        //TESTE MELHORESCOLHA
        double[] priorComposta = {0.2997, 0.2225, 0.0762, 0.4016};
        String respostaEsperada = "A alternativa escolhida é a alternativa: 4";

        String output = "";

        //TESTE CALCULORC
        int nMatNcons = 2;
        Matrix a = new Basic2DMatrix(matrizes[0]);
        Matrix b = new Basic2DMatrix(matrizes[1]);
        Matrix c = new Basic2DMatrix(matrizes[2]);
        Matrix d = new Basic2DMatrix(matrizCompCrit);
        EigenDecompositor eigenDa = new EigenDecompositor(a);
        EigenDecompositor eigenDb = new EigenDecompositor(b);
        EigenDecompositor eigenDc = new EigenDecompositor(c);
        EigenDecompositor eigenDd = new EigenDecompositor(d);
        Matrix[] matt1Crit = eigenDa.decompose();
        Matrix[] matt2Crit = eigenDb.decompose();
        Matrix[] matt3Crit = eigenDc.decompose();
        Matrix[] mattCompCrit = eigenDd.decompose();
        double matMaxValProp1Crit[][] = matt1Crit[1].toDenseMatrix().toArray();
        double matMaxValProp2Crit[][] = matt2Crit[1].toDenseMatrix().toArray();
        double matMaxValProp3Crit[][] = matt3Crit[1].toDenseMatrix().toArray();
        double matrMaxValPropCompCrit[][] = mattCompCrit[1].toDenseMatrix().toArray();

        //TESTE CALCULO IC 
        
        double expectedIC = 4;
        double maior = 6;
        int length = 2;
        double [][] matrizMaiorVal = matMaxValProp1Crit;
        //TESTE MAIORVALPROP
        
        double expectedMaiorValProp = 4.434692600847382;
        
        //TESTE nCrit (TOPSIS)
       String [] arrayS = {"BonJovi","Shakira",null,"FranciscoMoreira"}; 
        
        int expectednCrit =3;
        
        //OUTPUT
        if (testeDividir(string, expectedDouble)) {
            output += "%n%n`Dividir´: correto";

        } else {
            output += "%n%n`Dividir´: errado";
        }

        if (testeTesteConsist(expectedIC1, matriz1, cont, vetPrio1, outt, cabec) & testeTesteConsist(expectedIC2, matriz2, cont, vetPrio2, outt, cabec)) {
            output += "%n%n`TesteConsistCrit´: correto";

        } else {
            output += "%n%n`TesteConsistCrit´: errado";
        }

        if (testeConjuntoTestesConsist(expectedNmatrizNconsist, conta, guarda, matrizes, matrizPrioridades, vetPrioCrit, matrizCompCrit, normMatrizCompCrit, matNormCrit, priorCrit, outt, cabec)) {
            output += "%n%n`conjuntoTestesCrit´: correto";
        } else {
            output += "%n%n`conjuntoTestesCrit´: errado";

        }

        if (testeMelhorEscolha(respostaEsperada, matrizPrioridades, priorComposta, outt,pesoCrit)) {
            output += "%n%n`melhorEscolha´: correto";
        } else {
            output += "%n%n`melhorEscolha´: errado";

        }

        if (testeCalculoRC(nMatNcons, matMaxValProp1Crit, matMaxValProp2Crit, matMaxValProp3Crit, matrMaxValPropCompCrit, outt, cabec)) {
            output += "%n%n`calculoRC´: correto";
        } else {
            output += "%n%n`calculoRC´: errado";

        }

        if (testeCalculoIC(expectedIC, maior, length)) {
            output += "%n%n`calculoIC´: correto";
        } else {
            output += "%n%n`calculoIC´: errado";

        }
        
        if (testeMaiorValProp(expectedMaiorValProp, matrizMaiorVal)) {
            output += "%n%n`maiorValProp´: correto";
        } else {
            output += "%n%n`maiorValProp´: errado";

        }
        output += "%n%n-------------------TOPSIS-----------------------------------------------------------------------------------------------";
        if (testenCrit(expectednCrit, arrayS)) {
            output += "%n%n`nCrit´: correto";
        } else {
            output += "%n%n`nCrit´: errado";

        }
        //atraves destas incrementações conseguimos ter o output final (resultado dos testes) todos juntos.
        // pois se fizemos out.format("%n%n%s", "`metodo´: resultado"); para todas as respostas iria aparecer o processamento no meio dos resultados.
        outt.format("%n%n%s", "#########RESULTADO DOS TESTES##########################################################################################");
        outt.format(output);
        outt.format("%n%n"); // so para fazer um espaçamento da reposta do java.
        outt.close();
    }

    public static boolean testeDividir(String string, double expectedDouble) {
        double Double = dividir(string);
        return expectedDouble == Double;
    }

    public static boolean testeTesteConsist(boolean expectedIC, double[][] matriz, int[] cont, double[] vetPrio, Formatter out, String[] cabec) {
        boolean IC = testConsistCrit(matriz, cont, vetPrio, out, cabec,0.1);
        return expectedIC == IC;
    }

    public static boolean testeConjuntoTestesConsist(int expectedNmatrizNconsist, int[] cont, int guarda, double[][][] matrizes, double[][] matrizPrioridades, double[] vetPrioCrit, double[][] matrizCompCrit, double[][] normMatrizCompCrit, double[][][] matNormCrit, double[][] priorCrit, Formatter out, String[] cabec) {
        int nMatrizesNaoConsist = conjuntoTestesConsist(cont, matrizes, matrizPrioridades, vetPrioCrit, matrizCompCrit, normMatrizCompCrit, matNormCrit, priorCrit, out, cabec,0.1);
        return nMatrizesNaoConsist == expectedNmatrizNconsist;
    }

    public static boolean testeMelhorEscolha(String respostaEsperada, double[][] matrizPrioridades, double[] priorComposta, Formatter out,double[]pesoCrit) {
        String resposta = melhorEscolha(matrizPrioridades, priorComposta,pesoCrit);
        return resposta.equals(respostaEsperada);

    }

    public static boolean testeCalculoRC(int nMatNcons, double[][] matMaxValProp1Crit, double[][] matMaxValProp2Crit, double[][] matMaxValProp3Crit, double[][] matMaxValPropCompCrit, Formatter out, String[] cabec) {
        int nMatrizesNcons = calculoRCbib(matMaxValProp1Crit, matMaxValProp2Crit, matMaxValProp3Crit, matMaxValPropCompCrit, out, cabec,0.1);
        return nMatrizesNcons == nMatNcons;
    }

    public static boolean testeCalculoIC(double expectedIC, double maior, int length) {
        double IC = calculoIC(maior, length);
        return expectedIC == IC;
    }

    public static boolean testeMaiorValProp(double expectedMaiorValProp, double[][] matriz) {
        double maiorVal = maiorValProp(matriz);
        return maiorVal == expectedMaiorValProp;
    }
    public static boolean testenCrit( int exepctednCrit,String []arrayS) {
        double NnCrit = nCrit(arrayS);
        return NnCrit ==exepctednCrit;
    }
    
}
