package lapr1_turmadgh_grupo06;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;

/**
 *
 * @author Bruno Mendonça
 */
public class LAPR1_TurmaDGH_Grupo06 {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    //public final static String ficheiro_input = "ficheiro_input.txt";
    //public final static String ficheiro_input = "input_topsis.txt";
    //public final static String ficheiro_output = "ficheiro_output.txt";
    public static void main(String[] args) throws FileNotFoundException {
        String op = args[0];
        //String op = "M";
        //String op2 = "1";

        String ficheiro_input = args[2];
        String ficheiro_output = args[3];
        double limiar = 0;
        double limiarRC = 0.1;
        System.out.println("");
        System.out.println("");

        switch (op) {
            case "M": {

            }
            case "L": {

            }
            case "S": {

                String op2 = args[1];
                if (op.equals("L")) {
                    limiar = Double.parseDouble(op2);
                } else if (op.equals("S")) {
                    limiarRC = Double.parseDouble(op2);
                }

                double metd = Double.parseDouble(op2);
                int a = (int) Math.round(metd * 100);
                //desta maneira se os parametros forem por exemplo S 0.15 ele consegue correr
                // e se for M 2 ele consegue correr na mesma, pois todos os numeros passam a ser inteiros

                switch (a) {
                    default: {

                        String melhorEsc;
                        int resp;
                        int[] cont = new int[1];
                        cont[0] = 0;

                        int[] nCritOp = new int[2];
                        ahpCrit(ficheiro_input, nCritOp);
                        int N_CRITERIOS = nCritOp[0];
                        int N_OPCOES = nCritOp[1];
                        double matrizCompCrit[][] = new double[N_CRITERIOS][N_CRITERIOS];
                        double matrizes[][][] = new double[N_CRITERIOS][N_OPCOES][N_OPCOES];
                        //matrizes[0] -> estilo
                        //matrizes[1] -> confiabilidade
                        //matrizes[2] -> consumo
                        double priorComposta[] = new double[N_OPCOES];

                        //relativamente á matriz de comparação de criterios
                        double normMatrizCompCrit[][] = new double[N_CRITERIOS][N_CRITERIOS];
                        double vetPrioCrit[] = new double[N_CRITERIOS];

                        double matrizPrioridades[][] = new double[N_OPCOES][N_CRITERIOS];
                        //uma unica constituida pelos vetores prioridade de cada criterio, construida através do priorCrit...
                        double priorCrit[][] = new double[N_CRITERIOS][matrizes[0].length];
                        //tem os vetores prioridades de cada criterio
                        double matNormCrit[][][] = new double[N_CRITERIOS][matrizes[0].length][matrizes[0].length];
                        //contem as matrizes normalizadas de todos os criterios

                        //cabeçalho
                        String cabec[] = new String[N_CRITERIOS + 1];

                        lerMatriz(ficheiro_input, matrizCompCrit, matrizes, cabec);

                        do {

                            resp = menu();
                            switch (resp) {
                                case 1: {
                                    //sem utilizar a biblioteca

                                    double[] pesoCrit = new double[matrizCompCrit.length];

                                    normaliza(matrizCompCrit, normMatrizCompCrit);

                                    vetPrio(normMatrizCompCrit, pesoCrit);
                                    limiarCrit(pesoCrit, limiar);

                                    Formatter out = new Formatter(new File(ficheiro_output));
                                    Formatter consoleOut = new Formatter(System.out);

                                    int nMatNCons = conjuntoTestesConsist(cont, matrizes, matrizPrioridades, vetPrioCrit, matrizCompCrit, normMatrizCompCrit, matNormCrit, priorCrit, out, cabec, limiarRC);
                                    //calcula o numero de matrizes não consistentes
                                    if (nMatNCons == 0) {
                                        out.format("%n%n%s%n", "Todas as matrizes são consistentes (|RC|<" + limiarRC + ")");
                                        consoleOut.format("%n%n%s%n", "Todas as matrizes são consistentes (|RC|<" + limiarRC + ")");
                                    } else {
                                        out.format("%n%n%s%n", "Há " + nMatNCons + " matriz(/es) não consistentes (|RC|<" + limiarRC + ")");
                                        consoleOut.format("%n%n%s%n", "Há " + nMatNCons + " matriz(/es) não consistentes(|RC|<" + limiarRC + ")");
                                    }
                                    // informa dos resultados
                                    prioridadeComposta(matrizes, matrizPrioridades, matNormCrit, priorCrit);
                                    //normaliza as matrizes, guardando em matNormcrit,s
                                    //calcula os vetores priordade, guardado-os em priorCrit
                                    //depois de obter todos os vetores prioridades constroi a matrizPrioridades

                                    melhorEsc = melhorEscolha(matrizPrioridades, priorComposta, pesoCrit);
                                    //multiplica a matriz das prioridades pelo peso dos Criterios, obtendo a priorComposta
                                    //descobre qual o maior valor da prioridade composta e a quem pertence (Melhor escolha)
                                    cont[0] = 0;
                                    //inicia o contador de matrizes nao consistentes a 0

                                    output(out, consoleOut, normMatrizCompCrit, vetPrioCrit, matNormCrit, priorCrit, matrizCompCrit, matrizes, priorComposta, melhorEsc, cabec);
                                    // da o output da forma requesitada pelo cliente
                                    out.close();
                                    consoleOut.close();
                                    //fecha os formateres, escrevendo entao no local respetivo(fich/consola)
                                    break;

                                }
                                case 2: {
                                    //pela biblioteca
                                    Formatter out = new Formatter(new File(ficheiro_output));
                                    Formatter consoleOut = new Formatter(System.out);
                                    int nMatNCons = Bibl(matrizes, priorComposta, vetPrioCrit, priorCrit, matrizPrioridades, matrizCompCrit, out, matNormCrit, normMatrizCompCrit, cabec, N_CRITERIOS, N_OPCOES, limiarRC);
                                    // calcula o numero de matrizes nao consistentes
                                    //normaliza as matrizes e calcula os vetores prioridades
                                    // que depois guarda tambem na matrizPrioridades
                                    if (nMatNCons == 0) {
                                        out.format("%n%n%s%n", "Todas as matrizes são consistentes (|RC|<" + limiarRC + ")");
                                        consoleOut.format("%n%n%s%n", "Todas as matrizes são consistentes (|RC|<" + limiarRC + ")");
                                    } else {
                                        out.format("%n%n%s%n", "Há " + nMatNCons + " matriz(/es) não consistentes (|RC|<" + limiarRC + ")");
                                        consoleOut.format("%n%n%s%n", "Há " + nMatNCons + " matriz(/es) não consistentes(|RC|<" + limiarRC + ")");
                                    }
                                    //informa dos resultados
                                    double[] pesoCrit = new double[matrizCompCrit.length];

                                    normaliza(matrizCompCrit, normMatrizCompCrit);

                                    vetPrio(normMatrizCompCrit, pesoCrit);

                                    limiarCrit(pesoCrit, limiar);

                                    melhorEsc = melhorEscolha(matrizPrioridades, priorComposta, pesoCrit);
                                    //descobre e informa qual é a melhor escolha
                                    cont[0] = 0;
                                    //reinicia o contador de matrizes não nulas
                                    multMatrizVet(matrizPrioridades, pesoCrit, priorComposta);
                                    //faz o resto dos calculos necessarios para concluir o output
                                    output(out, consoleOut, normMatrizCompCrit, vetPrioCrit, matNormCrit, priorCrit, matrizCompCrit, matrizes, priorComposta, melhorEsc, cabec);
                                    //faz o output como requisitado pelo utilizador
                                    out.close();
                                    consoleOut.close();
                                    //feicha os formateres, escrevendo entao no local respetivo(fich/consola)
                                    break;
                                }
                                case 3: {
                                    testesUnitarios.main(args);
                                    //executa os testes unitarios
                                    break;
                                }
                                case 0: {
                                    break;
                                }

                            }

                        } while (resp != 0);

                        break;
                    }
//#########################################################################################################
//#################################TOPSIS##################################################################
//#########################################################################################################                    
                    case 200: {
                        System.out.printf("%s%n%s%n%s%n%s%n%n", "menu", "1-Topsis", "2-Testes", "0-fim");
                        boolean resposta = true;
                        Scanner inp = new Scanner(System.in);
                        int respo = inp.nextInt();
                        inp.close();
                        do {
                            if (resposta == false) {
                                Scanner inpu = new Scanner(System.in);
                                respo = inpu.nextInt();
                                inp.close();
                            }
                            if (respo == 1) {
                                int[] nCritAlt = new int[2];
                                nCritTopsis(ficheiro_input, nCritAlt);
                                int nCrit = nCritAlt[0];
                                int nAlt = nCritAlt[1];

                                String[][][] matrizCompleta = lerMatrizTopsis(ficheiro_input, nCrit, nAlt);
                                int[][] matrizInt = new int[nAlt][nCrit];
                                int[][] matrizIntQuad = new int[nAlt][nCrit];
                                //matriz inteira^2
                                double[] somatorio = new double[nCrit];
                                //raiz do somatorio de cada coluna da matriz inteira^2
                                double[][] matDivSom = new double[nAlt][nCrit];
                                //matriz normalizada 
                                double[][] matPesos = new double[nAlt][nCrit];
                                //matriz normalizada*peso dos critérios
                                double[][] matrizDistSolIdeal = new double[nAlt][nCrit];
                                //matriz com a distancia de cada escolha em relaçao á Sol Ideal 
                                double[][] matrizDistSolNegativa = new double[nAlt][nCrit];
                                double[] solIdeal = new double[nCrit];
                                double[] solIdealNegativa = new double[nCrit];
                                double[] siAst = new double[nAlt];
                                double[] siLinha = new double[nAlt];
                                int[] pos = new int[nAlt];
                                //posição da melhor/(es) escolhas

                                //matriz completa
                                //[0][0]-vetor bonsCrit
                                //[1][0]-vetor mausCrit
                                //[2][0]-vetor pesoCrit
                                //[3][0]-vetor alternativas
                                //[4]-matriz dos crit
                                int nBonsCrit = nCrit(matrizCompleta[0][0]);
                                int nMausCrit = nCrit(matrizCompleta[1][0]);
                                matStringParaInt(matrizCompleta[4], matrizInt);//passa a matriz para uma matriz Inteira (MatrizInt)
                                matAoQuadrado(matrizInt, matrizIntQuad);//eleva todos os elementos ao quadrado
                                somatorio(matrizIntQuad, somatorio);// faz o somatorio das colunas
                                divisaoPorSomatorio(matrizInt, somatorio, matDivSom);//divide cada elemento da coluna pelo somatorio, guarda na matDivSom
                                matrizXpesos(matDivSom, matrizCompleta[2][0], matPesos);//multiplica a matrizDivSom pelos pesos
                                solIdeal(matPesos, nBonsCrit, nMausCrit, solIdeal); // calcula a soluçaõ ideal
                                solIdealNegativa(matPesos, nBonsCrit, nMausCrit, solIdealNegativa);// calcula a sol. ideal negativa

                                //para obter siAst e siLinha temos de:
                                //modulo da soma das linhas da matrisDistancia(distancia de cada elemento á solIdeal)
                                //basicamente é a distancia total dos outros elemtentos ao elemento melhor para esse Crit
                                sIdeal(matPesos, solIdeal, siAst, matrizDistSolIdeal);//calcula siAst
                                sIdeal(matPesos, solIdealNegativa, siLinha, matrizDistSolNegativa);//calcula siLinha
                                melhorEsc(siAst, siLinha, pos);//descobre a melhor alternativa

                                outputTopsis(matrizCompleta, matDivSom, matPesos, solIdeal, solIdealNegativa, siAst, siLinha, pos, ficheiro_output, matrizDistSolIdeal, matrizDistSolNegativa);
                                // da o output da forma requesitada pelo cliente
                                break;
                            } else if (respo == 2) {
                                testesUnitarios.main(args);
                                break;
                            } else if (respo == 0) {
                                break;
                            }

                            resposta = false;
                            break;
                        } while (respo != 0);
                    }
                }
            }

        }

    }

    private static void limiarCrit(double[] pesoCrit, double limiar) {
        for (int i = 0; i < pesoCrit.length; i++) {
            if (pesoCrit[i] < limiar) {
                pesoCrit[i] = 0;
            }
        }
    }

    public static int menu() {
        System.out.println("Qual a operação?");
        System.out.println("1-Escolha utilizando o metodo simplificado");
        System.out.println("2-Escolha utilizando a biblioteca la4j");
        System.out.println("3-Testes de Metodos");
        System.out.println("0-Sair");
        Scanner input = new Scanner(System.in);

        int resp = input.nextInt();
        input.nextLine();
        return resp;
    }

    public static void lerMatriz(String ficheiro_input, double matrizCompCrit[][], double matrizes[][][], String cabec[]) throws FileNotFoundException {
        Scanner input = new Scanner(new File(ficheiro_input));
        int i = 0, l = 0, ultimaLinha = 1;

        while (input.hasNextLine()) {
            String linha = input.nextLine();
            if (linha.length() > 0) {
                if (i == 0) {
                    trataLinha(linha, l, matrizCompCrit, cabec, i);
                    l++;
                    ultimaLinha = 0;

                } else {
                    trataLinha(linha, l, matrizes[i - 1], cabec, i);
                    l++;
                    ultimaLinha = 0;
                }
            }
            if (linha.length() == 0 & ultimaLinha != 1) {
                l = 0;
                i++;
                ultimaLinha = 1;
                // Se a ultima linha era vazia ele não volta a incremtentar, 
                //de forma a preencher a matriz seguinte.
            }
        }

        input.close();

    }

    public static void ahpCrit(String ficheiro_input, int[] nCritOp) throws FileNotFoundException {
        //conta o numero de Criterios e de Opçoes, guardando no nCritOp
        Scanner input = new Scanner(new File(ficheiro_input));
        Formatter erro = new Formatter(new File("LogErros.txt"));
        int i = 0, l = 0, ultimaLinha = 1;
        nCritOp[1] = 0;
        while (input.hasNextLine()) {
            String linha = input.nextLine();
            if (linha.length() > 0) {// Se linha não está em branco trata-a
                String[] temp = linha.split("\\s+");
                if (i == 0 & l == 0) {
                    //por ex mcp_crit estilo conf cons; todos sao criterios menos o titulo
                    nCritOp[0] = temp.length - 1;
                    l++;
                    ultimaLinha = 0;

                } else if (l == 0) {
                    // mcp_alt_estilo  a b c d ; todos alternativas menos o titulo

                    if (nCritOp[1] == 0) {
                        nCritOp[1] = temp.length - 1;
                    } else if (nCritOp[1] != temp.length - 1) {
                        erro.format("%s%n", "Erro-Matrizes com numero diferente de alternativas ");
                        System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                        System.out.println("Os resultados poderão estar incorretos!");
                    }

                    l++;
                    ultimaLinha = 0;
                } else {

                    if (i > 0 & (temp.length != nCritOp[1])) {
                        erro.format("%s%d%s%d%s%n", "Erro - linha incompleta! matriz: ", i + 1, " linha: ", l, "   ");
                        System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                        System.out.println("Os resultados poderão estar incorretos!");
                    }
                    l++;
                }
            }
            if (linha.length() == 0 & ultimaLinha != 1) {

                if (i > 0 & l != nCritOp[1] + 1) {
                    erro.format("%s%d%s%n", "Erro - Matriz incompleta! (matriz: ", i + 1, ")  ");
                    System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                    System.out.println("Os resultados poderão estar incorretos!");
                }

                l = 0;
                i++;
                ultimaLinha = 1;
                // Se a ultima linha era vazia ele não volta a incremtentar, 
                //avançando de matriz só da primeira vez.
            }
        }
        if (i != (nCritOp[0])) {
            //se o numero total de matriz nao for igual ás matrizes de comparação de alternativas 
            //+1(matriz de comparação de critérios)
            erro.format("%s%n", "Erro-formato inválido");
            System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
            System.out.println("Os resultados poderão estar incorretos!");

        }
        input.close();
        erro.close();
        //fecha formatter de forma a escrever nos ficheiros
    }

    public static void trataLinha(String linha, int l, double[][] matriz, String[] cabec, int a) {
        String[] temp = linha.split("\\s+");
        // divide a string por qualquer numero de espaços consecutivos
        double c;
        if (l != 0) {
            for (int i = 0; i < temp.length; i++) {
                String b = temp[i].trim();
                if (b.indexOf('/') >= 0) {
                    //se tiver "/" é necessario divir, voltando já em double
                    c = dividir(b);
                } else {
                    c = Double.parseDouble(b);
                }
                matriz[l - 1][i] = c;
                //guarda na matriz correspondente
            }
        } else {
            //se for a primeira linha da matriz guarda só o cabeçalho
            cabec[a] = linha;
        }
    }

    public static void listagem(double[][] matriz, Formatter out) throws FileNotFoundException {
        //percorre matriz e envia para o formatter
        out.format("%n");

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                out.format("%.4f%s", matriz[i][j], "  ");

            }
            out.format("%n");

        }

    }

    public static void listagemMatrizDoubleCons(double[][] matriz) {

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.printf("%.4f%s", matriz[i][j], "  ");
            }
            System.out.printf("%n");
        }

    }

    public static void listagemMatrizStringConsola(String[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public static void listagemVetorStringConsola(String[] vetor) {
        for (int j = 0; j < vetor.length; j++) {
            System.out.print(vetor[j] + "  ");
        }
    }

    public static void listagemVetores(double[] vetor, Formatter out) throws FileNotFoundException {
        for (int i = 0; i < vetor.length; i++) {
            out.format("%n%.4f%s", vetor[i], "  ");
        }
        out.format("%n");
    }

    public static double dividir(String b) {
        String[] temp = b.split("/");
        //divide a string pela barra, passa de string para double e faz a divisão.
        double a = (Double.parseDouble(temp[0]) / Double.parseDouble(temp[1]));
        return a;
        //devolve a divisão já em double
    }

    public static void normaliza(double matriz[][], double[][] novMat) {
        double[] somatorio = new double[matriz[0].length];
        for (int i = 0; i < somatorio.length; i++) {
            somatorio[i] = 0;
            //inicia o somatorio a zeros.
        }
        for (int i = 0; i < somatorio.length; i++) {
            for (int j = 0; j < somatorio.length; j++) {
                somatorio[i] = somatorio[i] + matriz[j][i];
                // faz o somatorio de cada coluna
            }
        }
        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                novMat[i][j] = matriz[i][j] / somatorio[j];
                // divide cada coluna pelo somatorio de forma a normalizar a matriz
                //guardando entao a matriz normalizada numa outra matriz(novMat).
            }
        }
    }

    public static void vetPrio(double[][] matrizNorm, double[] vetPrio) {
        //vetor de prioridade é a media de cada linha da matriz normalizada
        double[] soma = new double[vetPrio.length];
        for (int i = 0; i < vetPrio.length; i++) {
            for (int j = 0; j < vetPrio.length; j++) {
                soma[i] = soma[i] + matrizNorm[i][j];
                //faz a soma de cada linha
            }
        }
        for (int i = 0; i < vetPrio.length; i++) {
            vetPrio[i] = soma[i] / vetPrio.length;
            //divide a soma de cada linha pelo numero total de linhas (MEDIA)
            //guarda os vetores prioridade 
        }
    }

    public static boolean testConsistCrit(double[][] matriz, int[] cont, double[] vetPrio, Formatter out, String[] cabec, double limiarRC) {

        double produto[] = new double[matriz.length];
        double soma = 0, maiorValProp;
        int i = 0;
        multMatrizVet(matriz, vetPrio, produto);

        for (int j = 0; j < produto.length; j++) {
            soma = soma + (produto[j] / vetPrio[j]);
        }
        maiorValProp = soma / produto.length;

        double IR[] = {0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41};
        double IC = (maiorValProp - matriz.length) / (matriz.length - 1);
        double IR1 = IR[matriz.length - 1];

        double RC = IC / IR1;
        //calcula o RC

        // dá o output
        cab(cont[0], out, cabec);
        cont[0]++;

        out.format("%n%s%.3f", "IR: ", IR1);
        out.format("%n%s%.3f", "RC: ", RC);
        out.format("%n%s%.3f", "Maior Valor Proprio: ", maiorValProp);
        return (RC < limiarRC & RC > -limiarRC);
        //se |RC| < limiarRC retorna true, se não retorna false.
    }

    public static void cab(int i, Formatter out, String[] cabec) {
        String[] partCrit = cabec[i].split("\\s+");
        String tit = partCrit[0];
        //mcp_crit est conf cons -> mcp_crit
        switch (i) {
            case 0: {
                out.format("%n%n%s", "Matriz de Comparação de Critérios");
                break;
            }
            default:
                out.format("%n%n%s", tit);
                break;
        }

    }

    public static void prioridadeComposta(double[][][] matrizes, double[][] matrizPrioridades, double[][][] matNormCrit, double[][] priorCrit) {

        for (int i = 0; i < matrizes.length; i++) {
            normaliza(matrizes[i], matNormCrit[i]);//normaliza as matrizes
            vetPrio(matNormCrit[i], priorCrit[i]);//calcula os vetores prioridades
        }

        for (int i = 0; i < matrizes.length; i++) {
            for (int j = 0; j < matrizes[0].length; j++) {
                matrizPrioridades[j][i] = priorCrit[i][j];//guarda os vetores prioridades na matrizPrioridades
            }
        }
    }

    public static String melhorEscolha(double[][] matrizPrioridades, double[] priorComposta, double[] pesoCrit) {
        String resposta = "";

        multMatrizVet(matrizPrioridades, pesoCrit, priorComposta);
        //calcula a prioridade composta
        double maior = 0;
        for (int i = 0; i < priorComposta.length; i++) {

            if (priorComposta[i] > maior) {
                maior = priorComposta[i];
            }

        }

        int pos = 0;
        boolean unico = true;
        for (int i = 0; i < priorComposta.length; i++) {
            double a = priorComposta[i];
            if (a == maior & unico == true) {
                pos = i + 1;
                unico = false;

                resposta = resposta + "A alternativa escolhida é a alternativa: " + pos;
            } else if (unico == false & priorComposta[i] == maior) {
                // já há um que correspode ao melhor, logo há mais do que uma hipotese.
                pos = i + 1;
                resposta = resposta + " / " + pos;

            }
        }

        return resposta;
    }

    public static void multMatrizVet(double[][] matriz, double[] vetor, double[] produto) {
        double soma = 0;
        if (vetor.length == matriz[0].length) {

            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[0].length; j++) {
                    soma = soma + (matriz[i][j] * vetor[j]);
                }
                produto[i] = soma;
                soma = 0;
            }
        } else {
            //se o vetor e matriz não forem da mesma dimensão ele cria um vetor que iguala o peso dos criterios
            double[] vetorNovo = new double[matriz[0].length];
            for (int i = 0; i < vetorNovo.length; i++) {
                vetorNovo[i] = (double) (1.0 / vetorNovo.length);
            }

            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[0].length; j++) {
                    soma = soma + (matriz[i][j] * vetorNovo[j]);
                }
                produto[i] = soma;
                soma = 0;
            }

        }
    }

    public static int conjuntoTestesConsist(int[] cont, double[][][] matrizes, double[][] matrizPrioridades, double[] vetPrioCrit, double[][] matrizCompCrit, double[][] normMatrizCompCrit, double[][][] matNormCrit, double[][] priorCrit, Formatter out, String[] cabec, double limiarRC) {
        int nMatNconsist = 0;

        normaliza(matrizCompCrit, normMatrizCompCrit);

        vetPrio(normMatrizCompCrit, vetPrioCrit);

        prioridadeComposta(matrizes, matrizPrioridades, matNormCrit, priorCrit);
        for (int j = 0; j < matrizes.length; j++) {
            for (int i = 0; i < matrizes[0].length; i++) {
                priorCrit[j][i] = matrizPrioridades[i][j];

            }
        }
        //teste consistencia da matriz de comparação de criterios
        if (testConsistCrit(matrizCompCrit, cont, vetPrioCrit, out, cabec, limiarRC) == false) {
            nMatNconsist++;
        }
        //teste consistencia da matriz de comparação de alternativas em relação a cada criterio
        for (int i = 0; i < matrizes.length; i++) {
            if (testConsistCrit(matrizes[i], cont, priorCrit[i], out, cabec, limiarRC) == false) {
                nMatNconsist++;
            }
        }

        return nMatNconsist;
        //devolve o número de matrizes não consistentes
    }

    public static int Bibl(double[][][] matrizes, double[] priorComposta, double[] vetPrioCrit, double[][] priorCrit, double[][] matrizPrioridades, double[][] matrizCompCrit, Formatter out, double[][][] matNormCrit, double[][] normMatrizCompCrit, String[] cabec, int N_CRITERIOS, int N_OPCOES, double limiarRC) {
        double[] prior1Crit = priorCrit[0];
        double[] prior2Crit = priorCrit[1];
        double[] prior3Crit = priorCrit[2];
        double[][] matNorm1Crit = matNormCrit[0];
        double[][] matNorm2Crit = matNormCrit[1];
        double[][] matNorm3Crit = matNormCrit[2];

        Matrix a = new Basic2DMatrix(matrizes[0]);
        Matrix b = new Basic2DMatrix(matrizes[1]);
        Matrix c = new Basic2DMatrix(matrizes[2]);
        Matrix d = new Basic2DMatrix(matrizCompCrit);
        EigenDecompositor eigenDa = new EigenDecompositor(a);
        EigenDecompositor eigenDb = new EigenDecompositor(b);
        EigenDecompositor eigenDc = new EigenDecompositor(c);
        EigenDecompositor eigenDd = new EigenDecompositor(d);
        Matrix[] matt1Crit = eigenDa.decompose();
        Matrix[] matt2Crit = eigenDb.decompose();
        Matrix[] matt3Crit = eigenDc.decompose();
        Matrix[] mattCompCrit = eigenDd.decompose();
        double matVet1Crit[][] = matt1Crit[0].toDenseMatrix().toArray();
        double matVet2Crit[][] = matt2Crit[0].toDenseMatrix().toArray();
        double matVet3Crit[][] = matt3Crit[0].toDenseMatrix().toArray();
        double matVetCompCrit[][] = mattCompCrit[0].toDenseMatrix().toArray();
        double matMaxValProp1Crit[][] = matt1Crit[1].toDenseMatrix().toArray();
        double matMaxValProp2Crit[][] = matt2Crit[1].toDenseMatrix().toArray();
        double matMaxValProp3Crit[][] = matt3Crit[1].toDenseMatrix().toArray();
        double matrMaxValPropCompCrit[][] = mattCompCrit[1].toDenseMatrix().toArray();

        vetorPrioMatr(vetPrioCrit, prior1Crit, prior2Crit, prior3Crit, matVetCompCrit, matVet1Crit, matVet2Crit, matVet3Crit, priorComposta, matrizPrioridades, N_CRITERIOS, N_OPCOES);

        int cont = calculoRCbib(matMaxValProp1Crit, matMaxValProp2Crit, matMaxValProp3Crit, matrMaxValPropCompCrit, out, cabec, limiarRC);

        //guarda as matrizes normalizadas, pelo metodo normal, visto que não é necessario fazer pela biblioteca,mesmo sendo um output obrigatorio.
        normaliza(matrizes[0], matNorm1Crit);
        vetPrio(matNorm1Crit, prior1Crit);
        normaliza(matrizes[1], matNorm2Crit);
        vetPrio(matNorm1Crit, prior2Crit);
        normaliza(matrizes[2], matNorm3Crit);
        vetPrio(matNorm3Crit, prior3Crit);
        normaliza(matrizCompCrit, normMatrizCompCrit);
        vetPrio(normMatrizCompCrit, vetPrioCrit);

        return cont;
    }

    public static void vetorPrioMatr(double[] vetPrioCrit, double[] prior1Crit, double[] prior2Crit, double[] prior3Crit, double[][] matVetCompCrit, double[][] matVet1Crit, double[][] matVet2Crit, double[][] matVet3Crit, double[] priorComposta, double[][] matrizPrioridades, int N_CRITERIOS, int N_OPCOES) {

        for (int i = 0; i < N_OPCOES; i++) {
            prior1Crit[i] = matVet1Crit[i][0];
        }
        if (prior1Crit[0] < 0) {
            for (int i = 0; i < N_OPCOES; i++) {
                //faz o modulo
                prior1Crit[i] = -prior1Crit[i];
            }
        }
        for (int i = 0; i < N_OPCOES; i++) {
            prior2Crit[i] = matVet2Crit[i][0];
        }
        if (prior2Crit[0] < 0) {
            for (int i = 0; i < N_OPCOES; i++) {
                //faz o modulo
                prior2Crit[i] = -prior2Crit[i];
            }
        }
        for (int i = 0; i < N_OPCOES; i++) {
            prior3Crit[i] = matVet3Crit[i][0];
        }
        if (prior3Crit[0] < 0) {
            for (int i = 0; i < N_OPCOES; i++) {
                //faz o modulo
                prior3Crit[i] = -prior3Crit[i];
            }
        }

        for (int i = 0; i < N_CRITERIOS; i++) {
            vetPrioCrit[i] = matVetCompCrit[i][0];
        }
        if (vetPrioCrit[0] < 0) {
            for (int i = 0; i < N_CRITERIOS; i++) {
                //faz o modulo
                vetPrioCrit[i] = -vetPrioCrit[i];
            }
        }

        //construi a matrizPrioridades
        for (int i = 0; i < N_OPCOES; i++) {
            matrizPrioridades[i][0] = prior1Crit[i];
            matrizPrioridades[i][1] = prior2Crit[i];
            matrizPrioridades[i][2] = prior3Crit[i];
        }

    }

    public static int calculoRCbib(double[][] matMaxValProp1Crit, double[][] matMaxValProp2Crit, double[][] matMaxValProp3Crit, double[][] matMaxValPropCompCrit, Formatter out, String[] cabec, double limiarRC) {
        int cont = 0;
        double maiorCrit, maior1Crit, maior2Crit, maior3Crit;
        double IR[] = {0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41};

        //calcula todos os RCs
        maiorCrit = maiorValProp(matMaxValPropCompCrit);
        double ICcrit = calculoIC(maiorCrit, matMaxValPropCompCrit.length);
        double IRcrit = IR[matMaxValPropCompCrit.length - 1];
        double RCcrit = (ICcrit / IRcrit);

        maior1Crit = maiorValProp(matMaxValProp1Crit);
        double IC1crit = calculoIC(maior1Crit, matMaxValProp1Crit.length);
        double IR1crit = IR[matMaxValProp1Crit.length - 1];
        double RC1crit = (IC1crit / IR1crit);

        maior2Crit = maiorValProp(matMaxValProp2Crit);
        double IC2crit = calculoIC(maior2Crit, matMaxValProp2Crit.length);
        double IR2crit = IR[matMaxValProp2Crit.length - 1];
        double RC2crit = (IC2crit / IR2crit);

        maior3Crit = maiorValProp(matMaxValProp3Crit);
        double IC3crit = calculoIC(maior3Crit, matMaxValProp3Crit.length);
        double IR3crit = IR[matMaxValProp3Crit.length - 1];
        double RC3crit = (IC3crit / IR3crit);

        // calcula o numero da matrizes nao consistentes(cont)
        if (RCcrit > limiarRC || RCcrit < -limiarRC) {
            cont++;
        }
        if (RC1crit > limiarRC || RC1crit < -limiarRC) {
            cont++;
        }
        if (RC2crit > limiarRC || RC2crit < -limiarRC) {
            cont++;
        }
        if (RC3crit > limiarRC || RC3crit < -limiarRC) {
            cont++;
        }

        escreverIC(0, out, cabec, IRcrit, RCcrit, maiorCrit);
        escreverIC(1, out, cabec, IR1crit, RC1crit, maior1Crit);
        escreverIC(2, out, cabec, IR2crit, RC2crit, maior2Crit);
        escreverIC(3, out, cabec, IR3crit, RC3crit, maior3Crit);
        return cont;
        // retorna o numero da matrizes nao consistentes(cont)
    }

    public static double maiorValProp(double[][] matriz) {
        //descobre o maior elemento da matriz
        double maior = 0;
        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[i][j] > maior) {
                    maior = matriz[i][j];
                }
            }
        }

        return maior;
    }

    public static double calculoIC(double maiorValProp, int length) {

        double IC = ((maiorValProp - length) / (length - 1));
        return IC;
    }

    public static void escreverIC(int i, Formatter out, String[] cabec, double IR, double RC, double maiorValProp) {
        cab(i, out, cabec);

        out.format("%n%s%.3f", "IR: ", IR);
        out.format("%n%s%.3f", "RC: ", RC);
        out.format("%n%s%.3f", "Maior Valor Proprio: ", maiorValProp);
    }

    public static void output(Formatter out, Formatter consoleOut, double[][] normMatrizCompCrit, double[] vetPrioCrit, double[][][] matNormCrit, double[][] priorCrit, double[][] matrizCompCrit, double[][][] matrizes, double[] priorComposta, String melhorEsc, String[] cabec) throws FileNotFoundException {
        String[] tit = new String[cabec.length];

        for (int i = 0; i < cabec.length; i++) {
            String[] partCrit = cabec[i].split("\\s+");
            tit[i] = partCrit[0];
        }

        out.format("%n%n%s%n", "Matriz normalizada da comparação de critérios");
        listagem(normMatrizCompCrit, out);
        out.format("%n%s%n", "Vetor Prioridade Critérios");
        listagemVetores(vetPrioCrit, out);
        //matrizes normalizadas de comparção das alternativas e respetivo vetor Prioridade
        for (int i = 0; i < tit.length - 1; i++) {
            out.format("%n%n%s%s%s%n", "Matriz ", tit[i + 1], " Normalizada");
            listagem(matNormCrit[i], out);
            out.format("%n%s%s%n", "Vetor Prioridade ", tit[i + 1]);
            listagemVetores(priorCrit[i], out);
        }

        //dados de input
        //matriz de comparação dos criterios
        out.format("%n%s%n", cabec[0]);
        consoleOut.format("%n%s%n", cabec[0]);
        listagem(matrizCompCrit, out);
        listagem(matrizCompCrit, consoleOut);
        //matrizes de e comparção das alternativas 
        for (int i = 0; i < matrizes.length; i++) {
            out.format("%n%s%n", cabec[i + 1]);
            consoleOut.format("%n%s%n", cabec[i + 1]);
            listagem(matrizes[i], out);
            listagem(matrizes[i], consoleOut);

        }

        out.format("%n%s%n", "Vetor Prioridade Composta");
        consoleOut.format("%n%s%n", "Vetor Prioridade Composta");
        listagemVetores(priorComposta, out);
        listagemVetores(priorComposta, consoleOut);

        out.format("%n%s%n%n%n%n", melhorEsc);
        consoleOut.format("%n%s%n%n%n%n", melhorEsc);
    }

//#######################################################################################################
//######################################### TOPSIS ######################################################
//#######################################################################################################
    public static String[][][] lerMatrizTopsis(String ficheiro_input, int nCrit, int nAlt) throws FileNotFoundException {
        Scanner input = new Scanner(new File(ficheiro_input));
        int linhaMat = 0, c = 0;
        int n = 0;
        if (nAlt > nCrit) {
            n = nAlt;
        } else {
            n = nCrit;
        }

        String[][][] guarda = new String[5][n][n];
        //matriz[x]
        //x: 0-bonsCrit 1-mausCrit 2-vet 3-alt 4-mat

        while (input.hasNextLine()) {
            String linha = input.nextLine();
            if (linha.length() > 0) {
                // Se linha não está em branco trata-a
                String[] temp = linha.split("\\s+");

                if (c == 0) {

                    for (int i = 0; i < temp.length - 1; i++) {
                        guarda[0][0][i] = temp[i + 1].trim();
                    }
                } else if (c == 1) {

                    for (int i = 0; i < temp.length - 1; i++) {
                        guarda[1][0][i] = temp[i + 1].trim();
                    }

                } else if (c == 3) {

                    for (int i = 0; i < temp.length; i++) {
                        guarda[2][0][i] = temp[i].trim();
                    }

                } else if (c == 2 || c == 4 || c == 5) {

                } else if (c == 6) {
                    for (int i = 0; i < temp.length - 1; i++) {
                        guarda[3][0][i] = temp[i + 1].trim();
                    }
                } else {
                    for (int i = 0; i < temp.length; i++) {
                        guarda[4][linhaMat][i] = temp[i].trim();

                    }
                    linhaMat++;
                }

                c++;
            }

        }

        input.close();

        return guarda;
    }

    public static void nCritTopsis(String ficheiro_input, int[] vetor) throws FileNotFoundException {
        Scanner input = new Scanner(new File(ficheiro_input));
        int n = 0, c = 0, a = 0;
        Formatter erro = new Formatter(new File("logErros.txt"));
        while (input.hasNextLine()) {
            String linha = input.nextLine();
            if (linha.length() > 0) {
                String[] temp = linha.split("\\s+");

                if (c == 0) {
                    n = temp.length - 1;
                } else if (c == 1) {

                    n = n + temp.length - 1;
                    vetor[0] = n;

                } else if (c == 2 & temp.length != n + 1) {
                    erro.format("%s%n", "vetor pesos - incorreto");
                    System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                    System.out.println("Os resultados poderão estar incorretos!");
                } else if (c == 3 & temp.length != n) {
                    erro.format("%s%n", "vetor pesos - incorreto");
                    System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                    System.out.println("Os resultados poderão estar incorretos!");
                } else if (c == 5 & temp.length != n + 1) {
                    erro.format("%s%n", "nº criterios incorreto");
                    System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                    System.out.println("Os resultados poderão estar incorretos!");
                } else if (c == 6) {
                    a = a + temp.length - 1;
                    vetor[1] = a;
                } else if (c > 6 & temp.length != a) {
                    erro.format("%s%n", "matriz incorreta, faltam criterios");
                    System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                    System.out.println("Os resultados poderão estar incorretos!");

                } else if (c > 6) {
                    for (int i = 0; i < temp.length; i++) {

                        try {
                            double d = Double.valueOf(temp[i].trim());
                            if (d == (int) d) {
                                //é inteiro
                            } else {
                                //é double
                                erro.format("%s%s%s%n", "numero não inteiro introduzido! (",temp[i].trim(),")");
                                System.out.println("ERRO! numero não inteiro introduzido! ("+temp[i].trim()+")");
                                System.out.printf("%s%n%n","Os resultados poderão estar incorretos!");
                            }
                        } catch (Exception e) {
                            erro.format("%s%n%d", "formato inválido, na matriz ", c);
                            System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
                            System.out.println("Os resultados poderão estar incorretos!");
                        }

                    }
                }
                c++;
            }
        }
        if (c != 7 + a) {
            erro.format("%s%n", "matriz incorreta, faltam alternativas");
            System.out.println("ERRO! veja o ficheiro logErros para mais informação.");
            System.out.println("Os resultados poderão estar incorretos!");
        }
        erro.close();
        input.close();

    }

    public static int nCrit(String[] array) {
        //conta o numero de palavras diferente de null
        int c = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                c++;
            }
        }
        return c;
    }

    public static void matStringParaInt(String[][] matrizString, int[][] matrizInt) {
        //passa tudo de string para int
        for (int i = 0; i < matrizInt.length; i++) {
            for (int j = 0; j < matrizInt[0].length; j++) {
                if (matrizString[i][j] == null) {
                    matrizInt[i][j] = -1;
                } else {
                    matrizInt[i][j] = Integer.parseInt(matrizString[i][j]);
                }
            }
        }
    }

    public static void matAoQuadrado(int[][] matriz, int[][] guarda) {
        //multiplica cada elemento por dois
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {

                guarda[i][j] = matriz[i][j] * matriz[i][j];
            }
        }
    }

    public static void somatorio(int matriz[][], double[] somatorio) {

        for (int i = 0; i < somatorio.length; i++) {
            somatorio[i] = 0;
            //inicia o somatorio a zeros.
        }
        for (int i = 0; i < somatorio.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                somatorio[i] = somatorio[i] + matriz[j][i];
                // faz o somatorio de cada coluna
            }
        }
        for (int i = 0; i < somatorio.length; i++) {

            somatorio[i] = Math.sqrt(somatorio[i]);

            // faz a raiz de cada elemento do samatorio
        }
    }

    public static void divisaoPorSomatorio(int matriz[][], double[] somatorio, double[][] matDivSom) {
        for (int i = 0; i < matDivSom[0].length; i++) {
            for (int j = 0; j < matDivSom.length; j++) {
                matDivSom[j][i] = matriz[j][i] / somatorio[i];
                // divide cada coluna pelo somatorio de forma a normalizar a matriz, guardando entao a matriz normalizada numa outra matriz.
            }
        }
    }

    public static void matrizXpesos(double[][] matriz, String[] pesos, double[][] matPesos) {
        double[] Dpesos = new double[matriz[0].length];
        for (int i = 0; i < matriz[0].length; i++) {
            Dpesos[i] = Double.parseDouble(pesos[i]);
            //passa os pesos dos criteriso de string para double
        }
        for (int c = 0; c < matriz[0].length; c++) {
            for (int l = 0; l < matriz.length; l++) {
                matPesos[l][c] = (matriz[l][c] * Dpesos[c]);
                //mutliplica a matriz pelos pesos e guarda na matPesos
            }
        }

    }

    public static void solIdeal(double matriz[][], int nBonsCrit, int nMausCrit, double[] Sol) {
//calcula a solução ideal
        int nCrit = nBonsCrit + nMausCrit;

        //descobre o maior dos bons criterios e guarda na posição respetiva na solução
        for (int i = 0; i < nBonsCrit; i++) {
            double maior = -1;
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[j][i] > maior) {
                    maior = matriz[j][i];
                }
            }
            Sol[i] = maior;

        }
//descobre o maior dos maus criterios e guarda na posição respetiva na solução
        for (int i = nBonsCrit; i < nCrit; i++) {
            double menor = 999;
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[j][i] < menor) {
                    menor = matriz[j][i];
                }
            }
            Sol[i] = menor;

        }

    }

    public static void solIdealNegativa(double matriz[][], int nBonsCrit, int nMausCrit, double[] Sol) {
        //calcula a solução ideal
        int nCrit = nBonsCrit + nMausCrit;

        //descobre o menor dos bons criterios e guarda na posição respetiva na solução
        for (int i = 0; i < nBonsCrit; i++) {
            double menor = 9999;
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[j][i] < menor) {
                    menor = matriz[j][i];
                }
            }
            Sol[i] = menor;

        }
//descobre o maior dos maus criterios e guarda na posição respetiva na solução
        for (int i = nBonsCrit; i < nCrit; i++) {
            double maior = -1;
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[j][i] > maior) {
                    maior = matriz[j][i];
                }
            }
            Sol[i] = maior;

        }

    }

    public static void sIdeal(double matriz[][], double[] Sol, double[] si, double matrizDist[][]) {
        double[][] matrizCop = new double[matriz.length][matriz[0].length];

        for (int i = 0; i < matriz.length; i++) {
            si[i] = 0;
        }
//  INICIA si A ZEROS.

        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matrizCop[j][i] = matriz[j][i];
            }
//cria uma copia da matriz
        }

        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matrizCop[j][i] = Math.abs(matrizCop[j][i] - Sol[i]);
            }
//SUBTRAI PELA SOL IDEALL
        }

        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matrizDist[j][i] = matrizCop[j][i];
            }
//Guarda na matriz da distancia
        }

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matrizCop[i][j] = matrizCop[i][j] * matrizCop[i][j];
            }
//MATRIZ DISTANCIA AO QUADRADO
        }

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                si[i] = matrizCop[i][j] + si[i];
            }
//SOMA DA LINHA E GUARDA NO SIAST
        }

        for (int i = 0; i < matriz.length; i++) {
            si[i] = Math.sqrt(si[i]);
//Faz a a raiz de cada elemento
        }

    }

    public static void melhorEsc(double[] siAst, double[] siLinha, int[] pos) {
        int a = 0;
        double maior = -1;
        pos[0] = -1;

        //Ci* = Si'/(Si*+Si')
        for (int i = 0; i < siAst.length; i++) {
            if ((siLinha[i] / ((siAst[i]) + siLinha[i])) > maior) {
                maior = (siLinha[i] / ((siAst[i]) + siLinha[i]));

            }
            // System.out.println((siLinha[i] / ((siAst[i]) + siLinha[i]))); 
        }

        for (int i = 0; i < siAst.length; i++) {
//calcula o maior do Ci', poi esse é a melhor escolha e nessa pos a prorpria pos, nos outros guarda -1
            if (maior == (siLinha[i] / ((siAst[i]) + siLinha[i]))) {
                pos[a] = i;
                a++;
            } else {
                pos[a] = -1;
                a++;
            }
        }

    }

    private static void outputTopsis(String[][][] matrizCompleta, double[][] matDivSom, double[][] matPesos, double[] solIdeal, double[] solIdealNegativa, double[] siAst, double[] siLinha, int[] pos, String ficheiro_output, double[][] matrizDistSolIdeal, double[][] matrizDistSolNegativa) throws FileNotFoundException {
        Formatter outFich = new Formatter(new File(ficheiro_output));
        Formatter outCons = new Formatter(System.out);

        //DADOS DE ENTRADA
        outFich.format("%n%n%s", "Peso dos Critérios");
        outCons.format("%n%n%s", "Peso dos Critérios");
        listagemVetoresString(matrizCompleta[2][0], outFich);
        listagemVetoresString(matrizCompleta[2][0], outCons);

        outFich.format("%n%n%s", "Matriz de decisão");
        outCons.format("%n%n%s", "Matriz de decisão");
        listagemMatrizString(matrizCompleta[4], outFich);
        listagemMatrizString(matrizCompleta[4], outCons);

        //MATRIEZES NORMALIZADAS
        outFich.format("%n%n%s", "Matriz normalizada");
        listagem(matDivSom, outFich);

        outFich.format("%n%n%s", "Matriz normalizada pesada");
        outCons.format("%n%n%s", "Matriz normalizada pesada");
        listagem(matPesos, outFich);
        listagem(matPesos, outCons);

        //SOLUÇOES IDEIAS
        outFich.format("%n%n%s", "Solução Ideal");
        listagemVetores(solIdeal, outFich);

        outFich.format("%n%n%s", "Solução Ideal Negativa");
        listagemVetores(solIdealNegativa, outFich);

        //distancia entre cada alternativa e as soluções,
        outFich.format("%n%n%s", "Distancia entre cada alternativa e a solucão ideal");
        listagem(matrizDistSolIdeal, outFich);

        outFich.format("%n%n%s", "Distancia entre cada alternativa e a solucão ideal negativa");
        listagem(matrizDistSolNegativa, outFich);

        //Vetor com as proximidades relativa à soluções
        outFich.format("%n%n%s", "Vetor com as proximidades relativa à solução ideal");
        outCons.format("%n%n%s", "Vetor com as proximidades relativa à solução ideal");
        listagemVetores(siAst, outFich);
        listagemVetores(siAst, outCons);

        outFich.format("%n%n%s", "Vetor com as proximidades relativa à solução ideal negativa");
        listagemVetores(siLinha, outFich);

        outFich.format("%n%n");
        outCons.format("%n%n");
        for (int i = 0; i < pos.length; i++) {

            if (pos[i] != -1) {
                //antes ele descobre o maior e percorre o resotultado, se for = ao maior
                // guarda a pos, se não guarda -1

                //MELHOR ALTERNATIVA
                outFich.format("%n%s%s%n", "A melhor alternativa é: ", matrizCompleta[3][0][i]);
                outCons.format("%n%s%s%n", "A melhor alternativa é: ", matrizCompleta[3][0][i]);
            }
        }
        outFich.format("%n%n%n%n");
        outCons.format("%n%n%n%n");
        outFich.close();
        outCons.close();

    }

    public static void listagemVetoresString(String[] vetor, Formatter out) {
        out.format("%n");
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] == null) {
                vetor[i] = "";
            }
            out.format("%s%s", vetor[i], " ");
        }

    }

    public static void listagemMatrizString(String[][] matriz, Formatter out) {
        out.format("%n");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j] == null) {
                    matriz[i][j] = "";
                }

                out.format("%s%s", matriz[i][j], "  ");

            }
            out.format("%n");

        }
    }
}
